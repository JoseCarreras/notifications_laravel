
<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <a class="btn btn-link" href="https://cevappdiscourse.dev.vanadis.es/login">Discourse</a>
    </div>
</div>

<form method="GET" action="/api/logout">
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-btn fa-sign-in"></i>Logout
            </button>
        </div>
    </div>
</form>
