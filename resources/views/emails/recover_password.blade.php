<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Recover Password</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body>
        <h1>Cambio de contraseña solicitado</h1>
        <p>Buenas.</p>
        <p>Nos ponemos en contacto contigo porque nos has solicitado un cambio de contraseña.</p>
        <p>Para iniciar sesión, deberás utilizar la siguiente contraseña</p>
        <p><b>{{ $password }}</b></p>
        <p>Un saludo.</p>
    </body>
</html>
