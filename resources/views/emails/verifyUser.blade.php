<!DOCTYPE html>
<html>
  <head>
    <title>Bienvenido</title>
  </head>
  <body>
    <h2>Bienvenido a la aplicación del CEV {{$user['name']}}</h2>
    <br/>
    Tu correo es {{$user['email']}} , Por favor pulsa en el enlace de abajo para verificar tu cuenta.
    <br/>
    <a href="{{url('api/user/verify', $user->verifyUser->token)}}">Verify Email</a>
  </body>
</html>