
## Introducción

Vanadis Template Laravel

- Laravel 8.40
- Backpack 4.1

## Despliegue Local

- Configurar Docker (LAMP)
- Clonar la rama master
- Instalar composer local a nivel de proyecto
- Lanzar composer update
- Configurar .env
- Lanzar migraciones (php artisan migrate)
- Lanzar seeders (php artisan db:seed)

## Despliegue Dev

- Lazar AMI de AWS (SB Laravel 2021 -> ami id: ami-00cb3359ec4913697)
- Clonar la rama master (/var/www)
- Instalar composer local a nivel de proyecto
- Lanzar composer update
- Configurar .env
- Lanzar migraciones (php artisan migrate)
- Lanzar seeders (php artisan db:seed)

## Acceso al Panel de Administración

- user: userdev
- email: userdev@vanadis.es
- password: Vanadis@00
