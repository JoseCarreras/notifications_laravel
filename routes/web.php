<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', function () {
    return view('login');
});

Route::get('/register', function () {
    return view('register');
});

Route::get('/discourse', function () {
    return view('discourse');
});

Route::get('/verify', function () {
    return view('account_confirmed');
});

//Route::get('/group', 'GroupController@addMembersToGroup');



Route::get('/sso/discourse', 'App\Http\Controllers\LoginController@login_discourse');


Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');



Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');


Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () {
    // routes to override others from Backpack
    Route::crud('user', 'UserCrudController');
});



