<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('/user/profile', 'App\Http\Controllers\UserController@profile');
Route::get('/user/course', 'App\Http\Controllers\UserController@course');
Route::get('/user/notifications', 'App\Http\Controllers\UserController@notifications');


Route::get('/user/recover/password', 'App\Http\Controllers\RecoverPasswordController@getEmailAndSendMail');
Route::post('/user/change/password', 'App\Http\Controllers\UserController@changePassword');
Route::get('/user/calendar', 'App\Http\Controllers\UserController@calendar');
Route::get('/user/news/', 'App\Http\Controllers\UserController@news');

Route::get('/user/verify/{token}', 'App\Http\Controllers\RegisterController@verifyUser');

Route::get('/recover/password/', 'App\Http\Controllers\RecoverPasswordController@recoverPassword');


Route::post('/firebase/token/save', 'App\Http\Controllers\UnregisteredUserController@createUnregisteredUser');
Route::post('/firebase/token/update', 'App\Http\Controllers\UnregisteredUserController@updateUnregisteredUser');
Route::delete('/firebase/token/unregistered/delete/{iid_token}', 'App\Http\Controllers\UnregisteredUserController@deleteUnregisteredUser');
Route::delete('/firebase/token/delete/{iid_token}', 'App\Http\Controllers\RegisterController@deleteUserToken');

Route::post('/register', 'App\Http\Controllers\RegisterController@register');
Route::post('/login', 'App\Http\Controllers\LoginController@login');
