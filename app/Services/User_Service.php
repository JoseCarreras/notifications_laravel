<?php

namespace App\Services;

use App\Models\UnregisteredUser;
use App\Models\User;


class User_Service
{
    private $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function deleteUserToken(){
        
        if ($this->user) {

            $this->user->iid_token = '';
            try
            {
                $this->user->save();
            }
            catch (\Throwable $th)
            {
                $response = $th;
                return $response;
            }

            $response = response()->json([
                'message' => 'Token eliminado',
            ], 200);
            return $response->header('status',"success");
        }
        $response = response()->json([
            'message' => 'Introduzca datos correctos',
        ], 422);
        return $response->header('status',"error");

    }

}