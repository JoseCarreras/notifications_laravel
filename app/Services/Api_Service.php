<?php

namespace App\Services;

use App\Helpers\Curl;

class Api_Service
{
    private $curl;
    private $email;

    public function __construct($email)
    {
        $curl = new Curl([
            'email' => $email,
            // 'url' => "https://script.google.com/macros/s/AKfycbw5ZFBZPJTSfldVIICOdY_K5YhsKEhPSN-H_Hm8x8M8JzARYkbm/exec"
            'url' => "https://script.google.com/macros/s/AKfycbxTYchYfMJO27yKL_rGmmDhZVv0iPhxDBXHkG_RIZl2IZLBeLWn/exec"

        ]);

        $this->curl = $curl;
    }

    public function home()
    {
        $response = $this->curl->profile();

        $home = [
            'name' => $response['response']['datos'][0]['nombre']
        ];

        return $home;
    }

    public function checkEmail()
    {
        $response = $this->curl->checkEmail();
        return $response;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function deleteUserToken(){
        
        if ($user) {

            $user->iid_token = '';
            try
            {
                $user->save();
            }
            catch (\Throwable $th)
            {
                $response = $th;
                return $response;
            }

            $response = response()->json([
                'message' => 'Token eliminado',
            ], 200);
            return $response->header('status',"success");
        }
        
        $response = response()->json([
            'message' => 'Introduzca datos correctos',
        ], 422);
        return $response->header('status',"error");
    }
}
