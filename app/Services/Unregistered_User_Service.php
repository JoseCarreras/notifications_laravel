<?php

namespace App\Services;

use App\Models\UnregisteredUser;


class Unregistered_User_Service
{
    private $iid_token;
    private $old_iid_token;

    public function __construct($iid_token, $old_iid_token)
    {
        $this->iid_token = $iid_token;
        $this->old_iid_token = $old_iid_token;
    }

    public function createUnregisteredUser()
    {
        $unregisteredUser = UnregisteredUser::where('iid_token', $this->iid_token)->first();
        if(!isset($unregisteredUser) ){

            $newUnregisteredUser = new UnregisteredUser();
            $newUnregisteredUser->iid_token = $this->iid_token;
            try
            {
                $newUnregisteredUser->save();
            }
            catch (\Throwable $th)
            {
                $response = $th;
                return $response;
            }

            $response = response()->json([
                'message' => 'Token registrado',
            ], 200);
            return $response->header('status',"success");

        }else {
            $response = response()->json([
                'message' => 'El token ya estaba registrado la',
            ], 400);
            return $response->header('status',"error");
        }
    }

    public function updateUnregisteredUser()
    {
        $unregisteredUser = UnregisteredUser::where('iid_token', $this->old_iid_token)->first();
        if(isset($unregisteredUser)){

            $unregisteredUser->iid_token = $this->iid_token;
            try
            {
                $unregisteredUser->save();
            }
            catch (\Throwable $th)
            {
                $response = response()->json([
                        'message' => 'No se pudo anadir el token',
                    ], 422);
                    return $response->header('status',"error");
            }

            $response = response()->json([
                'message' => 'Token actualizado',
            ], 200);
            return $response->header('status',"success");
            

        }
    }

    public function deleteUnregisteredUser()
    {
        $unregisteredUser = UnregisteredUser::where('iid_token', $this->iid_token)->first();
        if(isset($unregisteredUser) ){

            try
            {
                $unregisteredUser->delete();
            }
            catch (\Throwable $th)
            {
                $response = $th;
                return $response;
            }

            $response = response()->json([
                'message' => 'Token eliminado',
            ], 200);
            return $response->header('status',"success");

        }else {
            $response = response()->json([
                'message' => 'El token dado no existe',
            ], 400);
            return $response->header('status',"error");
        }
    }
}
