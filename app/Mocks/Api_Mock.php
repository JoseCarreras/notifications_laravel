<?php

namespace App\Mocks;

class Api_Mock
{
    public function profile()
    {
        return [
            'name' => 'Juan Rinconada',
            'id' => '1',
            'personalMail' => 'juanpruebas@gmail.com',
            'cevMail' => 'juanpruebas@cev.com',
            'authorities' => [
                'name' => 'ROLE_PROFESSOR'
            ],
            'token' => 'gsdgsdgfjrdg751uigff7gt25v9de1y8l122as58a12bjp8f2w',
            'courses' => [
                [
                    'id'=>'1',
                    'name' => 'FP Aplicaciones',
                    'subtitle' => 'Desarrollo de aplicaciones multiplataforma',
                    'type' => 'Presencial',
                    'destinationUrl' => 'https://www.cev.com/curso/tecnico-superior-en-desarrollo-de-aplicaciones-multiplataforma/',
                    'imageUrl' => 'https://www.cev.com/wp-content/uploads/2020/09/slider-tda.jpg', 
                ],

                [
                    'id'=>'2',
                    'name' => 'FP Videojuegos',
                    'subtitle' => 'Desarrollo de videojuegos',
                    'type' => 'Online',
                    'destinationUrl' => 'https://www.cev.com/curso/tecnico-superior-en-animaciones-3d-juegos-y-entornos-interactivos/',
                    'imageUrl' => 'https://www.cev.com/wp-content/uploads/2020/07/slideranimaciones3d-2.jpg',
                ]
            ],
        ];
    }

    public function course(){
        return 
        [
            [
                'id'=>'1',
                'name' => 'FP Aplicaciones',
                'subtitle' => 'Desarrollo de aplicaciones multiplataforma',
                'type' => 'Presencial',
                'destinationUrl' => 'https://www.cev.com/curso/tecnico-superior-en-desarrollo-de-aplicaciones-multiplataforma/',
                'imageUrl' => 'https://www.cev.com/wp-content/uploads/2020/09/slider-tda.jpg',
            ],

            [
                'id'=>'2',
                'name' => 'FP Videojuegos',
                'subtitle' => 'Desarrollo de aplicaciones multiplataforma',
                'type' => 'Online',
                'destinationUrl' => 'https://www.cev.com/curso/tecnico-superior-en-desarrollo-de-aplicaciones-multiplataforma/',
                'imageUrl' => 'https://www.cev.com/wp-content/uploads/2020/09/slider-tda.jpg',
            ]
                       
        ];
    }

    public function notification(){
        return [
            'title' => 'Bienvenido a CEV',
            'text' => 'Lorem ipsum',
            'timestamp' => '2021/05/24 19:51:40',
        ];
    }

    public function notifications(){
        return 
        [            
            [
                'title' => 'Bienvenido a CEV',
                'details' => [
                    'text' => 'Lorem ipsum',
                    'timestamp' => '2021/05/24 19:51:40',
                ]
            ],
            
            [
                'title' => 'Datos de interés',
                'details' => [
                    'text' => 'Lorem ipsum',
                    'timestamp' => '2021/04/12 12:25:37',
                ]
            ],            
        ];
    }
    
    public function recoverPassword(){
        return [
            'message' => 'Password recovery sent.'            
        ];
    }

    public function changePassword(){
        return [
            'message' => 'Password changed'            
        ];
    }

    public function calendar(){
        return[
            [
                'id' => '1',
                'fechaProg' => '20/02/2021',
                'eventType' => 'evento',
                'titulo' => 'Charlas programación',
                'horaIni' => '9:30',
                'horaFin' => '11:30',
                'profesor' => 'Julio Perez',
                'centro' => 'CEV Madrid',
                'aula' => '408',
            ],

            [
                'id' => '2',
                'fechaProg' => '20/02/2021',
                'eventType' => 'evento',
                'titulo' => 'Charlas programación',
                'horaIni' => '9:30',
                'horaFin' => '11:30',
                'profesor' => 'Julio Perez',
                'centro' => 'CEV Madrid',
                'aula' => '408',
            ],
        ];
    }

    public function news(){
        return[
            [
                'id' => '1',
                'title' => 'Bienvenido a CEV',
                'date' => '03/02/2021',
                'url' => 'https://www.cev.com',
                'imageUrl' => 'https://www.cev.com/wp-content/uploads/2019/11/2LOGO-CEV-CENTRO-DE-COMUNICACION.jpg',
            ],

            [
                'id' => '2',
                'title' => 'Nuevos grados disponibles',
                'date' => '05/07/2021',
                'url' => 'https://www.cev.com',
                'imageUrl' => 'https://www.cev.com/wp-content/uploads/2019/11/2LOGO-CEV-CENTRO-DE-COMUNICACION.jpg',
            ],
        ];
    }
}
