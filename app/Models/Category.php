<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;
    
    protected $fillable = ['name'];

    public function user(){
        return $this->belongsToMany(User::class, "user_categories");
    }

    public function notification()
    {
        return $this->belongsToMany(Notification::class);
    }
}
