<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Unregistered_User_Service;

class UnregisteredUserController extends Controller
{
    public function createUnregisteredUser(Request $request){

        if ($request->iid_token) {
            $api = new Unregistered_User_Service($request->iid_token, '');
            return $api->createUnregisteredUser();
        }

        $response = response()->json([
            'message' => 'Introduzca datos correctos',
        ], 422);
        return $response->header('status',"error");
        
    }

    public function updateUnregisteredUser(Request $request){

        if ($request->old_iid_token && $request->new_iid_token) {
            $api = new Unregistered_User_Service($request->new_iid_token, $request->old_iid_token);
            return $api->updateUnregisteredUser();
        }

        $response = response()->json([
            'message' => 'Introduzca datos correctos',
        ], 422);
        return $response->header('status',"error");
        
    }

    public function deleteUnregisteredUser($iid_token){

        if ($iid_token) {
            $api = new Unregistered_User_Service($iid_token, '');
            return $api->deleteUnregisteredUser();
        }

        $response = response()->json([
            'message' => 'Introduzca datos correctos',
        ], 422);
        return $response->header('status',"error");
        
    }

}
