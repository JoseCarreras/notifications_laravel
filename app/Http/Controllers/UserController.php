<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

use App\Mocks\Api_Mock;

use App\Services\Api_Service;

class UserController extends Controller
{
    private function get_api()
    {
        //return new Api_Service();
        return new Api_Mock();
    }

    public function profile(Request $request)
    {
        $api = $this->get_api();
        return $api->profile();
    }

    public function notifications(Request $request){
        $api = $this->get_api();
        return $api->notifications();
    }

    public function course(Request $request){
        $api = $this->get_api();
        return $api->course();
    }

    public function login(Request $request)
    {
        $api = $this->get_api();
        return $api->profile();
    }

    /*
    public function recoverPassword(Request $request)
    {
        $api = $this->get_api();
        return $api->recoverPassword();
    }
    */

    public function changePassword(Request $request)
    {
        $api = $this->get_api();
        return $api->changePassword();
    }

    public function calendar(Request $request)
    {
        $api = $this->get_api();
        return $api->calendar();
    }

    public function news(Request $request)
    {
        $api = $this->get_api();
        return $api->news();
    }

    public function deleteUserToken(Request $request)
    {
        $api = $this->get_api();
        return $api->news();
    }

    
}
