<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;

use App\Models\User;
use App\Models\VerifyUser;

use App\Helpers\Token;

use App\Mail\VerifyMail;

use App\Services\Api_Service;
use App\Services\Unregistered_User_Service;
use App\Services\User_Service;


class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $api = new Api_Service($request->cev_email);

        $response = $api->checkEmail();

        if($response['response'] != NULL){
            $user = new User();
            $user->email = $request->email;
            $user->cev_email = $request->cev_email;
            $user->password = $request->password;
            $user->name = $request->name;
            $user->iid_token = $request->iid_token;

            if ($request->iid_token) {
                $api = new Unregistered_User_Service($request->iid_token, '');
                $api->deleteUnregisteredUser();
            }

            try
            {
                $user->save();
            }
            catch (\Throwable $th)
            {
                if ($th->getCode() == "23000")
                {
                   $response = response()->json([
                        'message' => 'user exist',
                    ], 422);
                    return $response->header('status',"error");
                }
            }

            $verifyUser = VerifyUser::create([
                'user_id' => $user->id,
                'token' => sha1(time())
              ]);
              \Mail::to($user->email)->send(new VerifyMail($user));

            $response = response()->json([
                'message' => 'User registered now verify your email',
            ], 200);
            return $response->header('status',"success");
        }else{

            $response = response()->json([
                'message' => 'cev mail provided is not valid',
            ], 412);
            return $response->header('status',"error");
        }
    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
            $verifyUser->user->verified = 1;
            $verifyUser->user->save();
            $status = "Your e-mail is verified. You can now login.";
            } else {
            $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            return redirect('/')->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect('/verify')->with('status', $status);
    }

    public function deleteUserToken($iid_token){

        $user = User::where('iid_token', $iid_token)->first();

        if ($user) {
            $api = new User_Service($user);
            return $api->deleteUserToken();
        }
    }
}
