<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

use App\Mail\RecoverPassword;

use App\Helpers\PasswordGenerator;

class RecoverPasswordController extends Controller
{
   public function getEmailAndSendMail(Request $request){

        $user = User::where('email', $request->email)->first();
        if ($user == NULL)
        {
            return response()->json([
                'message' => 'Email no válido',
            ], 423);
        }
        $newPassword = new PasswordGenerator();

        $user->password = $newPassword->get_password_text_plain();

        try {
           $user->save();
           \Mail::to($user->email)->send(new RecoverPassword($newPassword));
        } catch (\Throwable $th) {
            $response = response()->json([
                'message' => 'Unexpected Error',
            ], 422);
            return $response->header('status',"error");  
        }

       

        return response()->json([
            'message' => 'Revisa tu correo para recuperar tu contraseña',
        ], 200);
   }

   public function recoverPassword($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            
        } else {
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect('/login')->with('status', $status);
    }
}
