<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;

use App\Services\Unregistered_User_Service;

use App\Helpers\Token;
use App\Helpers\Passwords;
use App\Helpers\PasswordGenerator;

use App\Mail\RecoverPassword;

use Cviebrock\DiscoursePHP\SSOHelper;



class LoginController extends Controller
{
    public function login(Request $request)
    {
        
        $user = User::where('email', $request->email)->first();
        if ($user == NULL)
        {
            return response()->json([
                'message' => 'Wrong user or password',
            ], 401);
        }

        if (Hash::check($request->password, $user->password))
        {
            
            if ($request->iid_token) {
                $user->iid_token = $request->iid_token;
                $api = new Unregistered_User_Service($request->iid_token, '');
                $api->deleteUnregisteredUser();
            }

            try
                {
                    $user->save();
                }
            catch (\Throwable $th)
            {
                $response = response()->json([
                    'message' => 'No se pudo actualizar el token correctamente',
                ], 422);
                return $response->header('status',"error");
            }
            $data_token = ['email' => $user->email];
            $token_builder = new Token($data_token);
            $token = $token_builder->encode();
            
            //setcookie("email", $user->email, time()+36000);

            //Prepara la respuesta para las necesidades de la aplicación
            $response = $this->prepareResponse($user, $token);

            return $response;
        }else{
            return response()->json([
                'message' => 'Wrong user or password',
            ], 401);
        }
        
    }

    public function prepareResponse($user, $token){
        $responseUser = [
            'id'=> $user->id,
            'name'=> $user->name,
            'email'=> $user->email,
            'cev_email'=> $user->cev_email,
            'iid_token'=> $user->iid_token,
            'authorities'=>[
                'name' => 'ROLE_STUDENT'
            ]
        ];
        $response = $responseUser;
        $response['token'] = $token;
        $response['courses'] = $this->getCourses();

       
        return $response;
    }

    public function getCourses(){
        return
         [
            [
                'state'=> 0,
                'name' => 'FP Aplicaciones',
                'location' => 'Madrid Cev',
                'image' => 'https://www.cev.com/wp-content/uploads/2020/09/slider-tda.jpg',
            ],

            [
                'state'=> 1,
                'name' => 'FP Videojuegos',
                'location' => 'Online',
                'image' => 'https://www.cev.com/wp-content/uploads/2020/09/slider-tda.jpg',
            ],
        ];
    }


    public function login_discourse(Request $request){

        Log::info($request);
        $sso = new SSOHelper;

        // this should be the same in your code and in your Discourse settings:
        $secret = 'hseefa5rgjuj81asyjk';
        $sso->setSecret( $secret );

        // load the payload passed in by Discourse
        $payload = $_GET['sso'];
        $signature = $_GET['sig'];
        
        // validate the payload
        if (!($sso->validatePayload($payload,$signature))) {
            // invaild, deny
            header("HTTP/1.1 403 Forbidden");
            echo("Bad SSO request");
            die();
        }
        
        $nonce = $sso->getNonce($payload);
        
        try {
            Log::info($_COOKIE);
            $email = $_COOKIE["email"];
        } catch (\Throwable $th) {
            var_dump($th->getMessage()); exit;
        }

        // Required and must be unique to your application
        $userId = $email;

        // Required and must be consistent with your application
        $userEmail = $email;
        
        // build query string and redirect back to the Discourse site
        $query = $sso->getSignInString($nonce, $userId, $userEmail);
        
        try{
            return Redirect::to('https://cevappdiscourse.dev.vanadis.es/session/sso_login?'. $query);
        }catch (\Throwable $th) {
            var_dump($th->getMessage()); exit;
        }
        
    }
}
