<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'users']);
        Permission::create(['name' => 'logs']);

        Role::create(['name' => 'superadmin'])
            ->givePermissionTo('users')
            ->givePermissionTo('logs');

        User::create([
            'name' => 'superadmin',
            'personal' => 'admin@vanadis.es',
            'password' => Hash::make('Vanadis@00'),
        ])
        ->assignRole('superadmin');

        User::create([
            'name' => 'userdev',
            'email' => 'userdev@vanadis.es',
            'password' => Hash::make('Vanadis@00'),
        ])
        ->assignRole('superadmin');
    }
}
